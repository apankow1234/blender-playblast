import os, re

class RenderDirectory :

	def __init__( self, path = ".", filename = "", lpad = 3 ) :
		self.path    = ""
		self.fn      = "" # no extension... just the name
		self.dirname = ""
		self.new     = False
		self.past    = []
		self.sep     = "-"
		self.lpad    = lpad
		self.dir     = None
		self.setPath( path )
		self.setFileName( filename )

	def getDir( self ) :
		if self.dir is not "" and self.new :
			self.makeDir()
		return self.dir

	def update( self ) :
		if self.path is not None and self.fn is not "" :
			self.fnLen = len( self.fn )
			self.getDirsWithBaseName()
			self.setDirName( self.dirname )
			self.dir = os.path.join( self.path, self.dirname )
		return self

	def setPath( self, path = "" ) :
		self.path = path;
		return self.update()

	def setFileName( self, filename = "" ) :
		# check for and remove extensions
		self.fn = filename
		return self.update()

	def getDirsWithBaseName( self ) :
		matches = []
		for dirname, dirnames, filenames in os.walk( self.path ):
			for subdirname in dirnames :
				if len( subdirname ) >= self.fnLen and subdirname[ 0:self.fnLen ] == self.fn :
					matches.append( subdirname )
		self.past = matches
		return self

	def getNextDirName( self ) :
		nums = []
		for i in range( len( self.past ) ) :
			suffix = self.past[ i ][ self.fnLen: ]
			if suffix is "" :
				continue
			test = re.match( ".*?(\d+)$", suffix )
			num = 0
			if( test ) :
				num = int( test.groups()[0] )
			nums.append( num )
		last = max( nums ) if nums else 0
		newSuffix = int( last ) + 1

		prefix = self.past[ 0 ][ 0:self.fnLen ] if self.past else self.fn 
		return prefix + self.sep + str( newSuffix ).zfill( self.lpad )

	def setDirName( self, dirname = "" ) :
		if dirname :
			self.dirname = dirname
		else :
			self.new     = True
			self.dirname = self.getNextDirName()
		return self

	def makeDir( self ) :
		pass

	def __str__( self ) :
		return os.path.join( self.path, self.dirname )


class Playblast :
	def __init__( self ) :
		pass

path = "__RENDERS/"
fn = "005"
pb   = RenderDirectory( path, fn )
print( pb.dir )